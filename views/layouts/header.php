<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="template/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="template/css/style.css">
<link rel="shortcut icon" href="/template/img/favicon.ico">
<script type="text/javascript" src="template/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="template/js/bootstrap.js"></script>
	<title>News+1</title>
</head>
<body>
<header>
    <a href="/"><img src="template/img/logo2.png" class="img-rounded"/></a>
</header>

<article>
<div class="login">
    <?php if(isset($_SESSION["admin"])):?>
    <a href="/admin_panel" class="btn btn-default"><i class="glyphicon glyphicon-user"></i> Админ панель</a>
    <?php else:?>
    <a href="/login" class="btn btn-default"><i class="glyphicon glyphicon-user"></i> Войти</a>
    <?php endif;?>
	
</div>