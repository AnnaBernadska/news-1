<?php include ROOT . '/views/site/admin/layouts/header.php'; ?>
         
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="table-responsive">
                <h1>Панель коментариев</h1>
                <table class="table table-striped" id="nav_table">
              <thead>
                <tr class="info">
                  <th>id<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>created<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>author<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>text<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>post_id<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>delete</th>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($comments as $value):?>
                <tr>
                  <td><?=$value["id"]?></td>
                  <td><?=$value["created"]?></td>
                  <td><?=$value["author"]?></td>
                  <td><?=$value["text"]?></td>
                  <td><?=$value["post_id"]?></td>
                   <td><a href="/admin/deleteComment-<?=$value["id"]?>" onclick="return confirm('Удалить коментарий?')" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></a></button></td>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
          </div>
<?php include ROOT . '/views/site/admin/layouts/footer.php'; ?>