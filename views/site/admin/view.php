<?php include ROOT . '/views/site/admin/layouts/header.php'; ?>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                    <h1><?= $post["post_title"] ?></h1>
                    <img src="/template/img/photos/<?= $post["id"] ?>.jpg" style="width: 90%;">
                    <p><?= $post["text"] ?></p>
                    <a href="/admin_panel"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>НАЗАД</a>

                    <h2>Коментарии:</h2>
                    <?php if (isset($comments)): ?>
                        <table class="table table-striped" id="nav_table">
                            <thead>
                                <tr class="info">
                                    <th>id</th>
                                    <th>author</th>
                                    <th>date</th>
                                    <th>text</th>
                                    <th>delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                        
                        <?php foreach ($comments as $comment): ?>
                            <tr>
                                <td><?= $comment["id"] ?></td>
                                <td><?= $comment["author"] ?></td>
                                <td><?= $comment["created"] ?></td>
                                <td><?= $comment["text"] ?></td>
                                <td><a href="/admin/deleteCommentByPost-<?= $post["id"] ?>/<?= $comment["id"] ?>" onclick="return confirm('Удалить файл?')" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></a></button></td>
                            </tr>
                        <?php endforeach; ?>
                            </tbody>
                            </table>
                    <?php endif; ?>
                    <br>
                </div>
<?php include ROOT . '/views/site/admin/layouts/footer.php'; ?>