<?php include ROOT . '/views/site/admin/layouts/header.php'; ?>
         
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="table-responsive">
                <h1>Панель новостей</h1>
                <a class="btn btn-primary" href="/admin/addPost" role="button" style="margin-bottom: 20px;">Добавить новый пост</a><br>
                
                <table class="table table-striped" id="nav_table">
              <thead>
                <tr class="info">
                  <th>id<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>title<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>date<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>description<br><span class="glyphicon glyphicon-sort"></span></th>
                  <th>view</th>
                  <th>edit</th>
                  <th>delete</th>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($news as $value):?>
                <tr>
                  <td><?=$value["id"]?></td>
                  <td><?=$value["post_title"]?></td>
                  <td><?=$value["post_created"]?></td>
                  <td><?=$value["description"]?></td>
                  <td><a href="/admin/post-<?=$value["id"]?>" type="button" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open"></a></button></td>
                  <td><a href="/admin/edit-<?=$value["id"]?>" type="button" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></a></button></td>
                  <td><a href="/admin/delete-<?=$value["id"]?>" onclick="return confirm('Удалить пост?')" type="button" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></a></button></td>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
          </div>
<?php include ROOT . '/views/site/admin/layouts/footer.php'; ?>