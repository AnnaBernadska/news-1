<?php include ROOT . '/views/site/admin/layouts/header.php'; ?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <script type="text/javascript" src="/template/js/bootstrap.js"></script>

    <div class="edit_post">
        <form action="/addNewPost/<?= $post["id"] ?>" method="POST" enctype="multipart/form-data">
                <label>Название</label>
                <input type="text" class="form-control"  name="title" required="true">

                <label>Изображение</label><br>
                
                <input type="file" name="image" >
                <br>
                <label>Краткое описание</label>
                <textarea  style="width: 90%;" name="description" ></textarea>
               
                <label>Текст</label>
                <textarea name="text" style="width: 90%;" ></textarea>
                
                <input type="submit" class="btn btn-default" style="margin-top: 10px;">
        </form>
        <br>
        <a href="/admin_panel"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>НАЗАД</a>
    </div>

</div>
<script type="text/javascript">
    bkLib.onDomLoaded(function () {
        nicEditors.allTextAreas()
    });
</script>
<?php include ROOT . '/views/site/admin/layouts/footer.php'; ?>
        