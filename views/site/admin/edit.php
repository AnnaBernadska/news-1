<?php include ROOT . '/views/site/admin/layouts/header.php'; ?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <script type="text/javascript" src="/template/js/bootstrap.js"></script>

    <div class="edit_post">
        <form action="/editPost/<?= $post["id"] ?>" method="POST" enctype="multipart/form-data">
            <label style="margin-top: 20px;">Название</label>
            <input type="text" class="form-control" value="<?= $post["post_title"] ?>" name="title" required>
            <label style="margin-top: 20px;">Изображение</label><br>
            <img src="/template/img/photos/<?= $post["id"] ?>.jpg" style="width: 30%;"/>
            <input type="file" name="image" required>
            <br>
            <label style="margin-top: 20px;">Краткое описание</label>
            <textarea  style="width: 90%;" name="description" required><?= $post["description"] ?></textarea>

            <label style="margin-top: 20px;">Текст</label>
            <textarea name="text" style="width: 90%;" required><?= $post["text"] ?></textarea>

            <input type="submit" class="btn btn-default" style="margin-top: 20px; margin-bottom: 20px;">
        </form>

        <a href="/admin_panel"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>НАЗАД</a>
    </div>

</div>
<script type="text/javascript">
    bkLib.onDomLoaded(function () {
        nicEditors.allTextAreas()
    });

</script>
<?php include ROOT . '/views/site/admin/layouts/footer.php'; ?>
        