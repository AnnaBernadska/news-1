<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="/template/img/favicon.ico">
        <script type="text/javascript" src="/template/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="/template/js/jquery.dataTables.min.js"></script>
        

        <script type="text/javascript" src="/template/js/nicEdit-latest.js"></script>
        <title>News+1 | Admin-panel</title>
        <!-- Bootstrap core CSS -->
        <link href="/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/template/css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="/template/css/dashboard.css" rel="stylesheet">

        <style id="holderjs-style" type="text/css"></style></head>
    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">News+1</a>
                </div>

                <div class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/logout">Выход</a></li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li><a href="/admin_panel">Новости</a></li>
                        <li><a href="/comments">Коментарии</a></li>
                    </ul>
                </div>
