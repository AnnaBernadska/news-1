<?php include ROOT . '/views/layouts/header.php'; ?>
<hr>

<section>
    <div class="post">
        <h6><?= $post["post_created"] ?></h6>
        <h1><?= $post["post_title"] ?></h1>
        <img class="img-thumbnail" src="template/img/photos/<?= $post["id"] ?>.jpg"/>
        <p><?= $post["text"] ?></p>
        

        <hr>
        <form action="javascript:void(null)" method="POST"  onsubmit="sendComment()">
            <legend>Введите коментарий</legend>
            <div class="form-group">
                <input type="text" class="form-control" id="author" placeholder="Имя" required>
                <textarea id="text" class="form-control" rows="3" placeholder="Текст коментария" required></textarea>
            </div>
            
            <button type="submit" class="btn btn-default">Отправить</button>
        </form>
        
        
        <div id="newComment"> </div>
        <?php if(isset($comments)):?>
        
        <?php foreach ($comments as $comment):?>
        <div class="comments">
            <h5><?=$comment["author"]?></h5>
            <h6><?=$comment["created"]?></h6>
            <?=$comment["text"]?>
        </div>
       
        <?php endforeach;?>
        <?php endif;?>
        <br>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        show();
        setInterval('show()', 1000);
    });
    function shdow() {
       var post_id = '<?php echo $id; ?>';
       $.ajax({
            url: '/components/comment.php',
            type: "POST",
            data: {
                post_id: post_id
            },
            success: function (data) {
                $('#newComment').append(data);
            },
            error: function () {
                alert("Some error occured");
            }
        });
    }
    function sendComment() {
        var author = $('#author').val();
        var text = $('#text').val();
        var post_id = '<?php echo $id; ?>';
        

        $.ajax({
            url: '/components/comment.php',
            type: "POST",
            data: {
                author: author,
                text: text,
                post_id: post_id
            },
            success: function (data) {
                $('#newComment').after(data);
                $('#author').val('');
                $('#text').val('');
            },
            error: function () {
                alert("Some error occured");
            }
        });
    }

</script>
<?php include ROOT . '/views/layouts/footer.php'; ?>