<?php

return array(
    'comments'=>'/admin/comments',
    
    'addNewPost' => '/admin/savePost', 
    'editPost/([0-9]+)' => '/admin/editPost/$1', 
    
    'admin/addPost' => '/admin/addPost', 
    'admin/edit-([0-9]+)' => '/admin/editPostById/$1',
    'admin/deleteComment-([0-9]+)' => '/admin/deleteCommentById/$1',
    'admin/deleteCommentByPost-([0-9]+)/([0-9]+)' => '/admin/deleteCommentByPost/$1/$2', 
    'admin/delete-([0-9]+)' => '/admin/deletePostById/$1', 
    'admin/post-([0-9]+)' => '/admin/viewPostById/$1',
    'admin_panel' => '/site/authorization',
   
    'logout' => '/site/logout',
    'login' => '/site/logIn', 
    
    'post-([0-9]+)' => '/site/post/$1', 
    
    'page-([0-9]+)' => '/site/index/$1', 
    
    'index.php' => '/site/index', 
    '/'=>'/site/index',
    ''=>'/site/index',
    );