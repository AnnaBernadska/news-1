<?php
define('ROOT', $_SERVER['DOCUMENT_ROOT']);

include_once ROOT.'/models/Comments.php';
require_once ROOT.'/components/Db.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $post_id = $_POST['post_id'];
    if(isset($_POST['text'])){
        $author = $_POST['author'];
        $text = $_POST['text'];
        $last_id = Comments::sendComment($author, $text, $post_id);
        if($last_id){
            $lastComment=Comments::getLastComment($last_id);
            echo '<div class="comments">
                <h5>'.$lastComment["author"].'</h5> 
                <h6>'.$lastComment["created"].'</h6>'.
                $lastComment["text"].'
                </div>';
        }else{
            'Ошибка отправки коментария';
        }
    }   
}
