<?php

class News {
    const POST_ON_PAGE = 5;

    public static function getNewsPostById($id){
        $db=Db::getConnection();
        $result=$db->query("select * from news where id=$id");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $post =$result->fetch();
        return $post;
    }
    public static function getNewsList(){
        $db=Db::getConnection();
        $newsList=array();
        $result=$db->query("select * from News order by post_created desc ");
                $result->setFetchMode(PDO::FETCH_ASSOC);
        for ($i=0;$row = $result->fetch();$i++) {
            $newsList[$i]['id']=$row ['id'];
            $newsList[$i]['post_title']=$row ['post_title'];
            $newsList[$i]['post_created']=$row ['post_created'];
            $newsList[$i]['description']=$row ['description'];
            $newsList[$i]['text']=$row ['text'];
        }
        
        return $newsList;
    }
    public static function getNewsListByPage($page=1){
        $offset=($page-1)*self::POST_ON_PAGE;
        $limit=  self::POST_ON_PAGE;
        $db=Db::getConnection();
        $newsList=array();
        $result=$db->query("select * from News order by post_created desc limit $limit offset $offset");
                $result->setFetchMode(PDO::FETCH_ASSOC);
        for ($i=0;$row = $result->fetch();$i++) {
            $newsList[$i]['id']=$row ['id'];
            $newsList[$i]['post_title']=$row ['post_title'];
            $newsList[$i]['post_created']=$row ['post_created'];
            $newsList[$i]['description']=$row ['description'];
            $newsList[$i]['text']=$row ['text'];
        }
        
        return $newsList;
    }
    public static function editPost($id,$title,$description,$text){
       $db = Db::getConnection();
        $sql = "UPDATE News
            SET 
                post_title = :post_title, 
                description = :description, 
                text = :text
            WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':post_title', $title);
        $result->bindParam(':description', $description);
        $result->bindParam(':text', $text);
        $result->bindParam(':id', $id);
        return $result->execute();
    }
    public static function deletePost($id){
        $db = Db::getConnection();
        $sql = 'DELETE FROM News WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    public static function addPost($title,$description,$text){
        $db = Db::getConnection();
        $sql = 'INSERT INTO News (post_title, description, text) '
                . 'VALUES (:post_title, :description, :text)';
        $result = $db->prepare($sql);
        $result->bindParam(':post_title', $title);
        $result->bindParam(':description', $description);
        $result->bindParam(':text', $text);
        $res=$result->execute();
        
        if($res){
            return $db->lastInsertId();
        }
    }
    public static function getTotalNews(){
        $db=Db::getConnection();
        $result=$db->query("SELECT COUNT(id) as c FROM `News`");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $count =$result->fetch();
        return $count['c'];
    }
}
