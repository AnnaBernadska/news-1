<?php

class Comments {
    public static function getComments($id){
        $db=Db::getConnection();
        $result=$db->query("select * from Comments where post_id=$id order by created desc");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $comments=array();
        for ($i=0;$row = $result->fetch();$i++) {
            $comments[$i]['id']=$row ['id'];
            $comments[$i]['post_id']=$row ['post_id'];
            $comments[$i]['created']=$row ['created'];
            $comments[$i]['author']=$row ['author'];
            $comments[$i]['text']=$row ['text'];
        }
        return $comments;
    }
     public static function sendComment($author, $text, $post_id){
        $db = Db::getConnection();
        $sql = 'INSERT INTO Comments (`post_id`,`author`,`text`) VALUES (:post_id,:author,:text)';   
        $result = $db->prepare($sql);
        $result->bindParam(':post_id', $post_id, PDO::PARAM_INT);
        $result->bindParam(':author', $author, PDO::PARAM_INT);
        $result->bindParam(':text', $text);
        $res= $result->execute();
          if($res){
              return $db->lastInsertId();
          }
     }
     public static function getLastComment($id){
        $db=Db::getConnection();
        $result=$db->query("select * from Comments where id=$id");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $comment =$result->fetch();
        return $comment;
     }
     public static function getCommentsList(){
        $db=Db::getConnection();
        $result=$db->query("select * from Comments");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $comments=array();
        for ($i=0;$row = $result->fetch();$i++) {
            $comments[$i]['id']=$row ['id'];
            $comments[$i]['post_id']=$row ['post_id'];
            $comments[$i]['created']=$row ['created'];
            $comments[$i]['author']=$row ['author'];
            $comments[$i]['text']=$row ['text'];
        }
        return $comments;
     }
     public static function deleteComment($id){
         $db = Db::getConnection();
        $sql = 'DELETE FROM Comments WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
     }
}
