<?php


class SiteController {

    public function actionIndex($page=1) {
        $news = News::getNewsListByPage($page);
        $total=  News::getTotalNews();
        $paginator=  new Pagination($total,$page,  News::POST_ON_PAGE,'page-');
        require_once (ROOT . '/views/site/index.php');
        return true;
    }

    public function actionPost($id) {
        $post = News::getNewsPostById($id);
        $comments = Comments::getComments($id);
        require_once (ROOT . '/views/site/view.php');
        return true;
    }

    public function actionLogIn() {
        if (isset($_SESSION["admin"])) {
            $news = News::getNewsList();
            require_once (ROOT . '/views/site/admin/admin_panel.php');
            return true;
        }  else {
            require_once (ROOT . '/views/site/login.php');
            return true;
        }
        
    }

    public function actionAuthorization() {
        if (isset($_SESSION["admin"])) {
            $news = News::getNewsList();
            require_once (ROOT . '/views/site/admin/admin_panel.php');
            return true;
        } else {
            $name = $_POST["name"];
            $password = $_POST["password"];
            if ($name == "admin" && $password == "admin") {
                $_SESSION["admin"]="admin";
                $news = News::getNewsList();
                require_once (ROOT . '/views/site/admin/admin_panel.php');
                return true;
            } else {
                header("Location: /");
            }
        }
    }
    public function actionLogout() {
        unset($_SESSION["admin"]);
        session_destroy();
        header("Location: /");
}
}