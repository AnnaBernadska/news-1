<?php

class AdminController {

    public function actionViewPostById($id) {
        if (isset($_SESSION["admin"])) {
            $post = News::getNewsPostById($id);
            $comments = Comments::getComments($id);
            require_once (ROOT . '/views/site/admin/view.php');
            return true;
        } else {
            header("Location: /");
        }
    }

    public function actionEditPostById($id) {
        if (isset($_SESSION["admin"])) {
            $post = News::getNewsPostById($id);
            require_once (ROOT . '/views/site/admin/edit.php');
            return true;
        } else {
            header("Location: /");
        }
    }

    public function actionDeletePostById($id) {
        if (isset($_SESSION["admin"])) {
            $post = News::deletePost($id);
            header("Location: /admin_panel");
        } else {
            header("Location: /");
        }
    }

    public function actionEditPost($id) {
        if (isset($_SESSION["admin"])) {
            $title = $_POST["title"];
            $description = $_POST["description"];
            $text = $_POST["text"];


            $res = News::editPost($id, $title, $description, $text);
            if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                //move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");

                $folder = "template/img/photos/{$id}.jpg";
                move_uploaded_file($_FILES["image"]["tmp_name"], $folder);
            }
            header("Location: /admin_panel");
        } else {
            header("Location: /");
        }
    }

    public function actionSavePost() {
        if (isset($_SESSION["admin"])) {
            $title = $_POST["title"];
            $description = $_POST["description"];
            $text = $_POST["text"];

            $lastId = News::addPost($title, $description, $text);
            if ($lastId) {
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    //move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");

                    $folder = "template/img/photos/{$lastId}.jpg";
                    move_uploaded_file($_FILES["image"]["tmp_name"], $folder);
                }
            }
            header("Location: /admin_panel");
        } else {
            header("Location: /");
        }
    }

    public function actionAddPost() {
        if (isset($_SESSION["admin"])) {
            require_once (ROOT . '/views/site/admin/add.php');
            return true;
        } else {
            header("Location: /");
        }
    }

    public function actionComments() {
        if (isset($_SESSION["admin"])) {
            $comments = Comments::getCommentsList();
            require_once (ROOT . '/views/site/admin/comments.php');
            return true;
        } else {
            header("Location: /");
        }
    }

    public function actionDeleteCommentById($id) {
        if (isset($_SESSION["admin"])) {
            Comments::deleteComment($id);
            header("Location: /comments");
        } else {
            header("Location: /");
        }
    }

    public function actionDeleteCommentByPost($postId, $id) {
        if (isset($_SESSION["admin"])) {
            Comments::deleteComment($id);
            header("Location: /admin/post-$postId");
        } else {
            header("Location: /");
        }
    }

}
